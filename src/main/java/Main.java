import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import test.Msg;

public class Main {

    @SuppressWarnings("resource")
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");
        Msg contextMsg = (Msg) context.getBean("msg");
        contextMsg.sandMsg();
//        System.out.println("-------------------------");
//        Msg msg1 = new Msg("wtf?");
//        Msg msg2 = new Msg("whaaaaaaaaaaaaat?");
//        msg1.sandMsg();
//        System.out.println("-------------------------");
//        msg2.sandMsg();
    }
}
