package test;

public class Msg {

    private String msg;

    public Msg() {
        this.msg = "default msg";
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Msg(String value) {
        System.out.println("Create MSG");
        this.msg = value;
    }

    public void sandMsg() {
        System.out.println("Sand MSG");
        System.out.println(msg);
    }

    public void sandMsg(String msg) {
        System.out.println(msg);
    }
}
