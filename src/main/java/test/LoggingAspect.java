package test;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class LoggingAspect {

    @Around("execution(* test.Msg.sandMsg (..))") //@Around("execution(* com.howtodoinjava.app.service.impl.EmployeeManagerImpl.createEmployee(..))")
    public void logSandMsgMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("execute logAroundGetEmployee");
        try {
            System.out.println("before aspect msg = " + ((Msg)joinPoint.getTarget()).getMsg());
            ((Msg)joinPoint.getTarget()).setMsg("ASDF!!!!!");
            joinPoint.proceed();
        } finally {
            System.out.println("after aspect msg = " + ((Msg)joinPoint.getTarget()).getMsg());
            //Do Something useful, If you have
        }
    }
}
